export interface Item {
  id: number;
  name: string;
  category: string;
  description: string;
  price: string;
  imageUrl: string;
  employeeCode: string;
}
